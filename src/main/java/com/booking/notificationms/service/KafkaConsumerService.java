package com.booking.notificationms.service;

import com.booking.notificationms.dto.KafkaDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class KafkaConsumerService {
private final ObjectMapper objectMapper;
private final EmailService emailService;
private final ResourceLoader resourceLoader;

    @KafkaListener(topics = "Notification",groupId = "ilyas")
    void listen (String data)  {
        try{KafkaDTO kafkaDTO = objectMapper.readValue(data, KafkaDTO.class);
            Resource resource = resourceLoader.getResource("classpath:/message/message.txt");

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8));
         String body =bufferedReader
                    .lines()
                    .map(line->line.contains("<a href=\"http://localhost:8080/api/v1/booking/confirm?token=your_token_here\" class=\"button\">")?
                            line="<a href=\"http://localhost:8080/api/v1/booking/confirm?token="+kafkaDTO.getConfirmationCode()+"\" class=\"button\">":line)
                    .collect(Collectors.joining(System.lineSeparator()));
            emailService.sendEmail(kafkaDTO.getUserEmail(),"Verify Your Booking"
                ,body);
            bufferedReader.close();

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }

    }
}
