package com.booking.notificationms.service;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;

import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;


@Service
@RequiredArgsConstructor
public class EmailService {


    private final JavaMailSender javaMailSender;
    private final ResourceLoader resourceLoader;
    public void sendEmail(String to, String subject, String body) throws MessagingException, IOException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(body, true);

        Resource resource1 = resourceLoader.getResource("classpath:/message/images.png");
        Path pathForImg = Paths.get(resource1.getFile().getAbsolutePath());
        helper.addInline("image",pathForImg.toFile());
        javaMailSender.send(message);
    }
}

