package com.booking.notificationms.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class KafkaDTO {
    private String userEmail;
    private String confirmationCode;
}
